import typer
from a import app as a
from b import app as b


app = typer.Typer()
app.add_typer(a, name='a')
app.add_typer(b, name='b')


@app.callback(invoke_without_command=True)
def main(ctx: typer.Context):
    """
    Example of a CLI app with two subcommands imported from external modules
    """
    if ctx.invoked_subcommand is None:
        print(f"About to execute command: {ctx.invoked_subcommand}")


if __name__ == "__main__":
    app()
