import typer


app = typer.Typer()


@app.callback(invoke_without_command=True)
def create(string: str):
    print(f"Some string: {string}")


if __name__ == "__main__":
    app()
