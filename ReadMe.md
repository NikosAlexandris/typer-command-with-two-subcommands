# CLI example of two sub-commands

Using [typer](https://typer.tiangolo.com/) to build a `command` with two sub-commands `a` and `b`:

- `command`:

    - `a`

        - some input: str

    - `b`

        - some input: str
